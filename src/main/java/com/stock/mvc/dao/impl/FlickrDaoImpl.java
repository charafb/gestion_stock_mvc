package com.stock.mvc.dao.impl;

import java.io.InputStream;

import javax.swing.JOptionPane;

import org.scribe.model.Token;
import org.scribe.model.Verifier;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.auth.AuthInterface;
import com.flickr4java.flickr.auth.Permission;
import com.flickr4java.flickr.uploader.UploadMetaData;
import com.stock.mvc.dao.IFlickrDao;

public class FlickrDaoImpl implements IFlickrDao {

	private Flickr flickr;

	private UploadMetaData uploadMetaData = new UploadMetaData();

	private String apiKey = "d94768b0cbde595bce72252caf7afc9a";

	private String sharedSecret = "0421c0069eb6ecae";

	private void connect() {
		flickr = new Flickr(apiKey, sharedSecret, new REST());
		Auth auth = new Auth();
		auth.setPermission(Permission.READ);
		auth.setToken("72157698855804582-85beef497bde1351");
		auth.setTokenSecret("430b936b009cdf30");
		RequestContext requestContext = RequestContext.getRequestContext();
		requestContext.setAuth(auth);
		flickr.setAuth(auth);
	}

//methode pour recupere l'url de notre photo enregistré dans le site flickr ,
	@Override
	public String savePhoto(InputStream photo, String title) throws Exception {
		connect();
		uploadMetaData.setTitle(title);
		String photoId = flickr.getUploader().upload(photo, uploadMetaData);
		return flickr.getPhotosInterface().getPhoto(photoId).getMedium640Url();
	}

	// methode pour se connecter au site flickr pour recupere le token et le
	// secretToken
	public void auth() {
		flickr = new Flickr(apiKey, sharedSecret, new REST());
		AuthInterface authInterface = flickr.getAuthInterface();

		Token token = authInterface.getRequestToken();
		System.out.println("token :" + token);

		String url = authInterface.getAuthorizationUrl(token, Permission.DELETE);
		System.out.println("Follow this URL to authorize yourself on Flickr");
		System.out.println(url);
		System.out.println("Paste in teh token it gives you:");
		System.out.println(">>");

		String tokenKey = JOptionPane.showInputDialog(null);

		Token requestToken = authInterface.getAccessToken(token, new Verifier(tokenKey));
		System.out.println("Authentification success");

		Auth auth = null;
		try {
			auth = authInterface.checkToken(requestToken);
		} catch (FlickrException e) {
			e.printStackTrace();

		}

		// this to ken can be used until the user revokes it.
		System.out.println("Token :" + requestToken.getToken());
		System.out.println("Secret :" + requestToken.getSecret());
		System.out.println("nsid :" + auth.getUser().getId());
		System.out.println("Realname :" + auth.getUser().getRealName());
		System.out.println("Username :" + auth.getUser().getUsername());
		System.out.println("Permission :" + auth.getPermission().getType());

	}

}

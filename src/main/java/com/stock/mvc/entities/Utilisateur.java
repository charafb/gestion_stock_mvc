package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Utilisateur implements Serializable {

    @Id
    @GeneratedValue
    private Long   idUtilisateur;

    private String nom;
    private String prenom;
    private String mail;
    private String motDepasse;
    private String photo;

    public Utilisateur() {
        super();
    }

    public Long getIdArticle() {
        return idUtilisateur;
    }

    public void setIdArticle( Long idArticle ) {
        this.idUtilisateur = idArticle;
    }

    public Long getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur( Long idUtilisateur ) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom( String nom ) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom( String prenom ) {
        this.prenom = prenom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail( String mail ) {
        this.mail = mail;
    }

    public String getMotDepasse() {
        return motDepasse;
    }

    public void setMotDepasse( String motDepasse ) {
        this.motDepasse = motDepasse;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto( String photo ) {
        this.photo = photo;
    }

}

package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneCommandeClient implements Serializable {

    @Id
    @GeneratedValue
    private Long           idLigneCmdClt;

    @ManyToOne
    @JoinColumn( name = "idArticle" )
    private Article        article;

    @ManyToOne
    @JoinColumn( name = "idCommandeClient" )
    private CommandeClient commandeClient;

    public LigneCommandeClient() {
        super();
    }

    public Long getIdArticle() {
        return idLigneCmdClt;
    }

    public void setIdArticle( Long idArticle ) {
        this.idLigneCmdClt = idArticle;
    }

    public Long getIdLigneCmdClt() {
        return idLigneCmdClt;
    }

    public void setIdLigneCmdClt( Long idLigneCmdClt ) {
        this.idLigneCmdClt = idLigneCmdClt;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle( Article article ) {
        this.article = article;
    }

    public CommandeClient getCommandeClient() {
        return commandeClient;
    }

    public void setCommandeClient( CommandeClient commandeClient ) {
        this.commandeClient = commandeClient;
    }

}
